var path = require('path');
var Nightmare = require('nightmare');
var should = require('chai').should();

describe('Login functionality end to end test', function () {
  this.timeout(60000);
  var url = 'http://localhost:3000';

  describe('Scenario 1', function () {

    it('User should be filled', function (done) {
      var nightmare = Nightmare({ show: true });

      nightmare
        .goto(url + '/login')
        .evaluate(function () {
          document.getElementById('input-user').value = '';
          document.getElementById('input-password').value = 'password';
        })
        .wait(3000)
        .evaluate(function() {
          document.getElementById('submitButton').click();
        })
        .wait(3000)
        .evaluate(function() {
          var result = {
            url: window.location.href
          }
          return result;
        })
        .end()
        .then(function (result) {
          result.url.should.equal('http://localhost:3000/login');
          done();
        })
        .catch(function (error) {
          console.error('An error has occurred. Make sure the development server is running!', error);
        });
    })

    it('Password should be filled', function (done) {
      var nightmare = Nightmare({ show: true });

      nightmare
        .goto(url + '/login')
        .evaluate(function () {
          document.getElementById('input-user').value = 'username';
          document.getElementById('input-password').value = '';
        })
        .wait(3000)
        .evaluate(function() {
          document.getElementById('submitButton').click();
        })
        .wait(3000)
        .evaluate(function() {
          var result = {
            url: window.location.href
          }
          return result;
        })
        .end()
        .then(function (result) {
          result.url.should.equal('http://localhost:3000/login');
          done();
        })
        .catch(function (error) {
          console.error('An error has occurred. Make sure the development server is running!', error);
        });
    });

    it('Correct user and password should be filled', function(done) {
      var nightmare = Nightmare({ show: true });

      nightmare
        .goto(url + '/login')
        .evaluate(function () {
          document.getElementById('input-user').value = 'rastan';
          document.getElementById('input-password').value = 'Pass123#';
        })
        .wait(3000)
        .evaluate(function() {
          document.getElementById('submitButton').click();
        })
        .wait(3000)
        .evaluate(function() {
          var result = {
            url: window.location.href
          }
          return result;
        })
        .end()
        .then(function (result) {
          result.url.should.equal('http://localhost:3000/');
          done();
        })
        .catch(function (error) {
          console.error('An error has occurred. Make sure the development server is running!', error);
        });
    })

    it('Username or password are incorrect', function(done) {
      var nightmare = Nightmare({ show: true });

      nightmare
        .goto(url + '/login')
        .evaluate(function () {
          document.getElementById('input-user').value = 'rastan';
          document.getElementById('input-password').value = 'password';
        })
        .wait(3000)
        .evaluate(function() {
          document.getElementById('submitButton').click();
        })
        .wait(3000)
        .evaluate(function() {
          var result = {
            url: window.location.href
          }
          return result;
        })
        .end()
        .then(function (result) {
          result.url.should.equal('http://localhost:3000/login');
          done();
        })
        .catch(function (error) {
          console.error('An error has occurred. Make sure the development server is running!', error);
        });
    })
  });
});
