FROM mhart/alpine-node:latest
ADD package.json tmp/package.json
RUN cd /tmp && npm install
RUN mkdir opt/app -p && cp -a /tmp/node_modules /opt/app

WORKDIR /opt/app
ADD . /opt/app

EXPOSE 8081