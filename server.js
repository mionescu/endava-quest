/*
 // sending to sender-client only
 socket.emit('message', "this is a test");

 // sending to all clients, include sender
 io.emit('message', "this is a test");

 // sending to all clients except sender
 socket.broadcast.emit('message', "this is a test");

 // sending to all clients in 'game' room(channel) except sender
 socket.broadcast.to('game').emit('message', 'nice game');

 // sending to all clients in 'game' room(channel), include sender
 io.in('game').emit('message', 'cool game');

 // sending to sender client, only if they are in 'game' room(channel)
 socket.to('game').emit('message', 'enjoy the game');

 // sending to all clients in namespace 'myNamespace', include sender
 io.of('myNamespace').emit('message', 'gg');

 // sending to individual socketid, but not sender
 socket.broadcast.to(socketid).emit('message', 'for your eyes only');
 */

var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io').listen(server);
var mongo = require('mongodb').MongoClient;
var path = require('path');
var jwt = require('jsonwebtoken');
var mapFormat = require('./js/server/format.js');
var gs = require('./js/server/GameServer.js').GameServer;
// For the binary protocol of update packets :
var CoDec = require('./js/CoDec.js').CoDec;
var Encoder = require('./js/server/Encoder.js').Encoder;
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var secret = 'eq-secret';
var utils = require('./lib/utils');
var sessionData = {};
var defaultQuestTarget = '480_7872';
var attributes = {};

server.enableBinary = true;
gs.server = server;

app.use('/css', express.static(__dirname + '/css'));
app.use('/js', express.static(__dirname + '/js'));
app.use('/assets', express.static(__dirname + '/assets'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(logger('dev'));
app.use(cookieParser());

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, '/views'));

process.title = 'endavaQuest';

app.get('/', function (req, res) {
    var token;
    if (!req.cookies || !req.cookies.token) {
        console.log('cookie or token not found', req.cookie && req.cookies.token)
        return res.redirect('/login');
    }

    token = utils.decodeToken(req.cookies.token);

    if (!token) {
        res.clearCookie('token');
        return res.redirect('/login');
    }

    gs.findIdByName(gs.users[token.user].name, function (err, data) {
        var id;
        var userData = {
            user: token.user,
            name: token.name,
            disciplineTitle: token.disciplineTitle,
            gender: token.gender,
            disciplineName: token.disciplineName,
            questTarget: defaultQuestTarget,
            users: JSON.stringify(gs.users),
            questions: [{question: {id: 1, name: 'question1', answers: [{ans: "ans1", correct: false}, {ans: "ans2", correct: true}, {ans: "ans3", correct: false}, {ans: "ans4", correct: false}]}}, {question: {id: 2, name: 'question2', answers: [{ans: "ans1", correct: false}, {ans: "ans2", correct: false}, {ans: "ans3", correct: false}, {ans: "ans4", correct: true}]}}]
        };

        attributes = {
            disciplineTitle: token.disciplineTitle,
            level: gs.users[token.user].level,
            picture: gs.users[token.user].picture
        }

        if (!err && data) {
            userData.playerid = data._id;
            userData.weapon = data.weapon;
            userData.armor = data.armor;
            userData.questTarget = data.questTarget || defaultQuestTarget;
        }

        return res.render('main', userData);
    });
});

app.get('/login', function (req, res) {
    var token;
    //check if cookie, if yes decode, if valid redirect to index, if no clear cookie. if !cookie send the login html.
    if (!req.cookies || !req.cookies.token) {
        return res.render('login', { title: 'Login Page' });
    }

    token = utils.decodeToken(req.cookies.token);

    if (!token) {
        res.clearCookie('token');
        return res.render('login', {});
    }

    return res.redirect('/');
});

app.post('/login', function (req, res) {
    var user;
    var password;
    var isValid = false;
    var token;
    if (!req.body || !req.body.user || !req.body.password) {
        return res.status('400').send('User or password missing');
    }

    user = req.body.user;
    password = req.body.password;
    isValid = validateLogin(gs.users, user, password);

    if (!isValid) {
        return res.status('403').send('Access denied. User or password incorrect.');
    }

    gs.findIdByName(gs.users[user].name, function (err, data) {
        var id;
        var dataToBeEncoded = {
            user: user,
            name: gs.users[user].name,
            disciplineTitle: gs.users[user].disciplineTitle,
            gender: gs.users[user].gender,
            disciplineName: gs.users[user].discipline
        };
        if (!err && data) {
            dataToBeEncoded.playerid = data._id;
            dataToBeEncoded.weapon = data.weapon;
            dataToBeEncoded.armor = data.armor;
            sessionData[gs.users[user].name] = data.questTarget;
        }

        console.log('data to be encoded is:', dataToBeEncoded);
        token = utils.encodeToken(dataToBeEncoded, '30d');

        res.cookie('token', token, { maxAge: 1000 * 60 * 60 * 24 * 29 });//29 days
        res.status(200).send('Access granted.');
    });

});

function validateLogin(users, user, password) {
    if (!users[user] || users[user].password !== password) return false;
    return true;
}

// Manage command line arguments
var myArgs = require('optimist').argv;
var mongoHost, mongoDBName;

function sleep(milliseconds) {
    console.log('Waiting for database - start: ' + new Date().getTime());
    var start = new Date().getTime();
    while (true) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
    console.log('Waiting for database - finished: ' + (new Date().getTime() - start));
}

if (myArgs.waitForDatabase) {
    sleep(myArgs.waitForDatabase);
}

var mongoPort = (myArgs.mongoPort || 27017);
var mongoServer = (myArgs.mongoServer || 'localhost');
console.log(myArgs.mongoServer);

mongoHost = mongoServer + ':' + mongoPort;
mongoDBName = 'phaserQuest';

server.listen(myArgs.p || process.env.PORT || 3000, function () { // -p flag to specify port ; the env variable is needed for Heroku
    console.log('Listening on ' + server.address().port);
    server.clientUpdateRate = 1000 / 5; // Rate at which update packets are sent
    gs.readMap();
    server.setUpdateLoop();

    mongo.connect('mongodb://' + mongoHost + '/' + mongoDBName, function (err, db) {
        if (err) throw (err);
        server.db = db;
        console.log('Connection to db established');
    });
});

io.on('connection', function (socket) {
    console.log('connection with ID ' + socket.id);
    console.log(server.getNbConnected() + ' already connected');
    socket.pings = [];

    socket.on('ponq', function (sentStamp) {
        // Compute a running estimate of the latency of a client each time an interaction takes place between client and server
        // The running estimate is the median of the last 20 sampled values
        var ss = server.getShortStamp();
        var delta = (ss - sentStamp) / 2;
        if (delta < 0) delta = 0;
        socket.pings.push(delta); // socket.pings is the list of the 20 last latencies
        if (socket.pings.length > 20) socket.pings.shift(); // keep the size down to 20
        socket.latency = server.quickMedian(socket.pings.slice(0)); // quickMedian used the quickselect algorithm to compute the median of a list of values
    });

    socket.on('init-world', function (data) {
        if (!gs.mapReady) {
            socket.emit('wait');
            return;
        }

        if (data.new) {
            if (!gs.checkSocketID(socket.id)) return;
            gs.addNewPlayer(socket, data);
        } else {
            if (!gs.checkPlayerID(data.id)) return;
            gs.loadPlayer(socket, data.id, attributes);
        }
    });

    socket.on('revive', function () {
        gs.revivePlayer(gs.getPlayerID(socket.id));
    });

    socket.on('path', function (data) {
        if (!gs.handlePath(data.path, data.action, data.or, socket)) socket.emit('reset', gs.getCurrentPosition(socket.id));
    });

    socket.on('newQuestTarget', function (data) {
        console.log('I have a new quest target', data.questTarget);
        gs.updateQuestTarget(data);
        // GameServer.deletePlayer = function (id) {
        //     GameServer.server.db.collection('players').deleteOne({ _id: new ObjectId(id) }, function (err) {
        //         if (err) throw err;
        //     });
        // };

    });

    socket.on('chat', function (txt) {
        if (!txt.length || txt.length > 300) return;
        var rooms = gs.listAOIsFromSocket(socket.id);
        var playerID = gs.getPlayerID(socket.id);
        rooms.forEach(function (room) {
            socket.broadcast.to(room).emit('chat', { id: playerID, txt: txt });
        });
    });

    socket.on('delete', function (data) {
        gs.deletePlayer(data.id);
    });

    socket.on('disconnect', function () {
        console.log('Disconnection with ID ' + socket.id);
        if (gs.getPlayer(socket.id)) gs.removePlayer(socket.id);
    });

});

server.setUpdateLoop = function () {
    setInterval(gs.updatePlayers, server.clientUpdateRate);
};

server.sendInitializationPacket = function (socket, packet) {
    packet = server.addStamp(packet);
    if (server.enableBinary) packet = Encoder.encode(packet, CoDec.initializationSchema);
    socket.emit('init', packet);
};

server.sendUpdate = function (socketID, pkg) {
    pkg = server.addStamp(pkg);
    try {
        pkg.latency = Math.floor(server.getSocket(socketID).latency);
    } catch (e) {
        console.log(e);
        pkg.latency = 0;
    }
    if (server.enableBinary) pkg = Encoder.encode(pkg, CoDec.finalUpdateSchema);
    if (pkg) io.in(socketID).emit('update', pkg);
};

server.getNbConnected = function () {
    return Object.keys(gs.players).length;
};

server.addToRoom = function (socketID, room) {
    var socket = server.getSocket(socketID);
    socket.join(room);
};

server.leaveRoom = function (socketID, room) {
    var socket = server.getSocket(socketID);
    if (socket) socket.leave(room);
};

server.sendID = function (socket, playerID) {
    socket.emit('pid', playerID);
};

server.sendError = function (socket) {
    socket.emit('dbError');
};

server.addStamp = function (pkg) {
    pkg.stamp = server.getShortStamp();
    return pkg;
};

server.getShortStamp = utils.getShortStamp;

server.getSocket = function (id) {
    return io.sockets.connected[id]; // won't work if the socket is subscribed to a namespace, because the namsepace will be part of the id
};

server.quickMedian = utils.median;