var jwt = require('jsonwebtoken');
var secret = 'eq-secret';
var quickselect = require('quickselect'); // Used to compute the median for latency

String.prototype.insert = function (index, string) {
  if (index > 0)
    return this.substring(0, index) + string + this.substring(index, this.length);
  else
    return string + this;
};

function decodeToken(token) {
  var payload;
  try {
    payload = jwt.verify(token, secret);
  } catch (err) {
    console.log(`Validation error occurred! Error body: ${err}`);
    return false;
  }
  return payload;
}

function encodeToken(data, expires) {
  if (!expires) {
    return jwt.sign(data, secret);
  }
  return jwt.sign(data, secret, { expiresIn: expires });
}
function median(arr) { // Compute the median of an array using the quickselect algorithm
  var l = arr.length;
  var n = (l % 2 == 0 ? (l / 2) - 1 : (l - 1) / 2);
  quickselect(arr, n);
  return arr[n];
};

function getShortStamp() {
  return parseInt(Date.now().toString().substr(-9));
};

function validatePassword(password) {
  if (!password || !/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,30}$/.test(password)) return false;
  return true;
}

module.exports = {
  decodeToken: decodeToken,
  encodeToken: encodeToken,
  median: median,
  getShortStamp: getShortStamp
}