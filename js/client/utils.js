window.utils = (function () {

  var messageContainer = {
    init: function (container) {
      this.container = container || $('.message-container');
      return this;
    },
    show: function () {
      if(!this.container) return console.error('message container not found');
      this.container.removeClass('hidden');
    }, 
    hide: function () {
      if(!this.container) return console.error('message container not found');
      this.container.addClass('hidden');      
    },
    message: function (msg) {
      if(!msg || !this.container) return console.error('message container or message are missing');
      this.container.text(msg);
    }
  };

  function validatePassword(password) {
    if (!password || !/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,30}$/.test(password)) return false;
    return true;
  }

  return {
    messageContainer: messageContainer,
    validatePassword: validatePassword
  }
}());

