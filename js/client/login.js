(function () {
  'use strict';
  var loginContainer = $('.login-container');
  var user = loginContainer.find('#input-user');
  var password = loginContainer.find('#input-password');
  var loginButton = loginContainer.find('.btn-login');
  var utils = window.utils;
  var loginEndpoint = '/login';
  var messageContainer = utils.messageContainer.init($('.message-container'));
  
  function init() {
    bindEvents();
  }

  function bindEvents() {
    //could use delegation here
    user.on('keypress', enterHandler);

    password.on('keypress', enterHandler);

    loginButton.on('click', function (ev) {
      if (!validateLoginFields()) {
        return false;
      }
      $.post(loginEndpoint, { user: user.val(), password: password.val() })
        .done(function (response) {
          console.log(response)
          window.location.href = '/';
        })
        .fail(function (err) {
          console.log('error is:', err);
          messageContainer.show();
          clearLoginFields();
          return messageContainer.message(err.responseText || err.response);
        })
      return false;
    });
  }

  function clearLoginFields() {
    user.val('');
    password.val('');
  }

  function validateLoginFields() {
    var userVal = user.val();
    var passVal = password.val();
    if (!userVal || userVal.length < 3) return highlightField(user);
    removeHighlight(user);
    if (!passVal || passVal.length < 3 || !utils.validatePassword(passVal)) return highlightField(password);
    removeHighlight(password);
    return true;
  }

  function removeHighlight(field) {
    field.removeClass('error-highlight');
  }

  function highlightField(field) {
    field.addClass('error-highlight');
  }

  function enterHandler(e) {
    var key = e.which;
    if (key == 13) {
      loginButton.click();
      return false;
    }
  }

  init();
}());