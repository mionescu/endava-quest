var logoutButton = $('.logout');
var book = $('.book');
var preview = $('.preview');
var card = $('.card');
var previewAnouncement = $('.preview-anouncement');
var previewAnounmentOpen = false;
var game = new Phaser.Game(980, 500,
    (navigator.userAgent.toLowerCase().indexOf('firefox') > -1 ? Phaser.CANVAS : Phaser.AUTO),
    document.getElementById('game'), null, true, false);

game.state.add('Home', Home);
game.state.add('Game', Game);
game.state.start('Home');

function bindEvents() {
    $(document).on('click', '.card', function (ev) {
        if (previewAnounmentOpen || ev.target.classList.contains('close')) {
            return previewAnouncement.hide();
        }
        var target = ev.target.classList.contains('card') ? $(ev.target) : $(ev.target).parent();
        var title = target.data('title');
        var text = target.data('description');

        previewAnouncement.find('h3').html(title);
        previewAnouncement.find('p').html(text);
        previewAnouncement.show('fast');
        previewAnounmentOpen = true;
        previewAnouncement.addClass(target.attr('class'));
    });

    previewAnouncement.find('.close').on('click', function () {
        previewAnounmentOpen = false;
        previewAnouncement.hide('fast');
    });

    book.hover(function (e) {
        console.log(e);
        var image = $(e.target).css('background');
        preview.css({ background: image });
        preview.show();
    }, function (e) {
        preview.hide();
    });
    logoutButton.on('click', function () {
        console.log('clicked logout');
        document.cookie = ('token=; expires=Thu, 01 Jan 1970 00:00:00 UTC');
        clearLocalStorage();
        window.location = '/login';
    });
}


function clearLocalStorage() {
    localStorage.clear();
}

bindEvents();